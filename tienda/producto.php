<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="sass/estilos.css"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php
        require "componentes.php";
        require "datos.php";
        session_start();
        cargarCavecera();
        if(isset($_GET['producto'])){
            extract($_GET);
            $producto=$productos[$producto];
            $srcImagen=$producto['imagen'];
            if($producto['imagen']==''){
                $srcImagen="imagenes/imagenPorDefecto.png";
            }
        }
        if(isset($_POST['aCarrito'])&&$_POST['cantidad']>0){
            $array=['codigo'=>$_GET['producto'],"cantidad"=>$_POST['cantidad']];
            if(isset($_SESSION['carrito'])) {
            array_push($_SESSION['carrito'],$array);
            header("Location:index.php");
            die;
            }
            
        }
    ?>
    <title><?=$productos[$_GET['producto']]['nombre'] ?></title>
</head>
<body>
    <div class="container">
        <div class="container float-left">
            <img id="imagenProductos" src="<?=$srcImagen?>">
        </div>
        <div class="container float-right">
            <p class="h2"><?=$producto['nombre']?></p>
            <p class="h1"><?=$producto['precio']?>€</p>
            <form method="post">
                <input type="numeric" name="cantidad" >
                <button class="btn" type="submit" name="aCarrito">Añadir a carrito</button>
            </forn>
        </div>
    </div>
    
</body>
</html>