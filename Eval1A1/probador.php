<?php
session_start();

/**
 * Control de entrada de prendas en probadores
 *
 * Parámetros Acción
 * -------------------------------------------------
 * ...        Añade una prenda al probador N
 * ...        Disminuye una prenda en el probador N
 * ...        Vacía el probador N
 * ...        Vacia todos los probadores
 *
 */

if (isset($_POST['numprobadores']) && isset($_POST['bnumprob'])) {
    for ($i = 0; $i < $_POST['numprobadores']; $i++) {
        array_push($_SESSION['probadores'], ['numprob' => $i, 'nprendas' => 0]);
    }
}

if(isset($_POST['anyadirPrenda'])){
    $_SESSION['probadores'][$_POST['anyadirPrenda']]['nprendas']++;
}
else;
if(isset($_POST['eliminarPrenda'])){
    $_SESSION['probadores'][$_POST['eliminarPrenda']]['nprendas']--;
}
if(isset($_POST['vaciarProb'])){
    $_SESSION['probadores'][$_POST['vaciarProb']]['nprendas']=0;
}
//var_dump($_SESSION['probadores']);

require 'vista.php';
if (isset($_SESSION['probadores'])) {
    mostrarProbadores();
}
